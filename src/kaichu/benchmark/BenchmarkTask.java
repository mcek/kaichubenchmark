/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kaichu.benchmark;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.KaichuJob;
import kaichu.KaichuParameters;
import kaichu.KaichuResponse;
import kaichu.KaichuTask;

/**
 *
 * @author maciektr
 */
public class BenchmarkTask implements KaichuTask {

    @Override
    public KaichuResponse execute(KaichuParameters param) {
	// retrieve parameters
	BenchmarkParameters params = (BenchmarkParameters)param;
	
	// do the work
	try {
	    //System.out.println("Waiting for " + 30);
	    Thread.sleep(KaichuBenchmark.WAIT_TIME);
	} catch(InterruptedException ex) {
	    Logger.getLogger(KaichuBenchmark.class.getName()).log(Level.SEVERE, null, ex);
	}

	InetAddress address = null;

	try {
	    address = InetAddress.getLocalHost();
	} catch(UnknownHostException ex) {
	    Logger.getLogger(KaichuBenchmark.class.getName()).log(Level.SEVERE, null, ex);
	}

	// create and return the response
	return new BenchmarkResponse("Hello! I'm " + 
		address + " and I've just slept for " + KaichuBenchmark.WAIT_TIME + " ms!");
    }

}
