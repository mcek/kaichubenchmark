/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kaichu.benchmark;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import kaichu.KaichuJob;
import kaichu.KaichuResponse;
import kaichu.KaichuUtility;
import kaichu.master.ActiveWorkersListener;
import kaichu.master.DispatchQueue;
import kaichu.master.KaichuManager;
import kaichu.master.TaskRegistry;

/**
 *
 * @author maciektr
 */
public class KaichuBenchmark implements ActiveWorkersListener {

    /**
     * @param args the command line arguments
     */
    
    static Random random = new Random();
    
    int numActiveWorkers = 0;
    
    final static int WAIT_TIME = 30000;
    
    boolean clean;
    
    public static void main(String[] args) {
        boolean debug = false;
        for(String arg : args) {
            if (arg.equalsIgnoreCase("-debug")) {
                debug = true;
            }
        }
	new KaichuBenchmark(Integer.parseInt(args[0]), debug);
    }
	
    public KaichuBenchmark(int numWorkers, boolean debug) {
	
	System.out.println("Job time: " + KaichuBenchmark.WAIT_TIME);

	// initialise
	KaichuManager manager = new KaichuManager();

	KaichuManager.setDebug(debug);

	TaskRegistry.registerTask(new BenchmarkTask(), manager);

	ArrayList<byte[]> numBytes = new ArrayList<byte[]>();
	
	manager.activeWorkersListener = this;

	//numBytes.add(make(10));
	numBytes.add(make(1024*5));
	numBytes.add(make(1024*50));
	numBytes.add(make(1024*500));
	numBytes.add(make(1024*5000));
	//numBytes.add(make(1024 * 1024));
	//numBytes.add(make(10 * 1024 * 1024));

	//System.console().readLine("Press enter to run");
	
	//int numWorkers = 600;

	boolean go = true;

	while(go) {

	    for(byte[] bytes : numBytes) {
		
		System.out.println(KaichuUtility.getDate() + " Starting new round...");

		while(numActiveWorkers != numWorkers) {
		    try {
			Thread.sleep(1000);
		    } catch(InterruptedException ex) {
			Logger.getLogger(KaichuBenchmark.class.getName()).log(Level.SEVERE, null, ex);
		    }
		}
		
		System.out.println(KaichuUtility.getDate() + " Go for " + bytes.length + " bytes!");
		
		clean = true;

		// submit jobs
		DispatchQueue queue = manager.getDispatchQueue();

		long totalTime = 0;
		int numJobs = 10 * numWorkers;
                

		for(int i = 0; i < numJobs; i++) {

		    //HelloParameters parameters = new HelloParameters(random.nextInt(2000) + 2000);
		    BenchmarkParameters parameters = new BenchmarkParameters(bytes);

		    KaichuJob job = new KaichuJob(parameters, BenchmarkTask.class);
		    queue.submitJob(job);

		    //System.out.println("Job " + i + " with sleeping time " + parameters.sleepTime + " created.");
		    totalTime += WAIT_TIME;
                    
		}

		manager.start();

		// retrieve responses
		long start = System.currentTimeMillis();

		for(int i = 0; i < numJobs; i++) {
		    try {
			KaichuResponse respc = manager.getNextResponse();
		    } catch(InterruptedException ex) {
			Logger.getLogger(KaichuBenchmark.class.getName()).log(Level.SEVERE, null, ex);
		    }
		    
		    if(!clean) {
			System.out.println(KaichuUtility.getDate() + " Cancelling jobs...");
			manager.cancelJobs();
			System.out.println(KaichuUtility.getDate() + " Done cancelling jobs!");
			break;
		    }

		    //System.out.println("Response " + respc.getID() + " returned: " + ((HelloResponse)respc.response).message);
		}

		System.out.println(KaichuUtility.getDate() + " $$$$$$$$$$$$$$$$$$$ Done in " + (System.currentTimeMillis() - start) / 1000.0 + "s instead of " + totalTime / 1000.0 + " for " + bytes.length + " bytes");
		if(clean) {
		    System.out.println("Good!");
		    //Toolkit.getDefaultToolkit().beep();
		}
		manager.nextRound();
	    }
	}

	//System.console().readLine("Press enter");

	manager.terminate();
    }
    
    private static byte[] make(int size) {
	byte[] bytes = new byte[size];
	random.nextBytes(bytes);
	return bytes;
    }
    

    @Override
    public void numWorkersChanged(int numWorkers) {
	this.numActiveWorkers = numWorkers;
	clean = false;
    }
}
